prefix?=/usr

.PHONY: install

install:
	mkdir -pv "$(DESTDIR)$(prefix)/include"
	mkdir -pv "$(DESTDIR)$(prefix)/lib"
	cp -v "include/orbital.h" "$(DESTDIR)$(prefix)/include"
	cp -v "target/$(HOST)/release/liborbital.a" "$(DESTDIR)$(prefix)/lib"
	cp -v "target/$(HOST)/release/liborbital.so" "$(DESTDIR)$(prefix)/lib"

